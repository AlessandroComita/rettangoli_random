//  TRACCIA

    //  creare una pagina html dove al click del mouse si crei un rettangolo di dimensione random e di colore random

    //  la dimensione varia da 50px a 400px sia per x che per y

    //  i colori variano da red, yellow, green, purple, blue, brown, black

    //  l'angolo superiore sinistro dovrà coincidere con punto del mouse dove si clicca

// ESTENSIONI
    // numerare i rettangoli man mano che si creano
    // eliminare i rettagnoli al click


// contiene il dato relativo all'altezza del box superiore della pagina
// per poterlo compensare al rilevamento della posizione del puntatore
var boxSuperiore = 125;
    
// questa variabile contiene il numero complessivo progressivo dei rettangoli
let rettangolo_numero = 1;

// questa variabile serve a rilevare se l'utente clicca sulla cornice vuota
// all'inizio la cornice è vuota, quindi la variabile viene inizializzata con "true"
let vuoto = true;

// «aggancia» la cornice in cui cliccare per far apparire i rettangoli
const cornice = document.querySelector('#cornice');


// questa funzione gestisce il disegno del rettangolo
function disegnaRettangolo()
{
    // rileva posizione del puntatore del mouse
    var ascissa = event.clientX;
    var ordinata = event.clientY;

    // inizializza massimo e minimo in base alle dimensioni richieste
    // dalla traccia
    let max = 400;
    let min = 50;
    
    // questa variabile serve per la individuazione del colore random
    // ed esprimere il numero di colori tra i quali scegliere
    let numero_colori = 7;
    
    // mettiamo in un array tutti i colori richiesti dalla traccia
    // così il programma andrà a scegliere uno di questi
    // in base all'esito del "sorteggio" randomico
    let colori_richiesti = ['red', 'yellow', 'green', 'purple', 'blue', 'brown', 'gray'];
            
    // sorteggia colore
    let colore_random = Math.round(Math.random() * (numero_colori-1));

    // assegna il colore in base all'esito della randomizzazione
    let colore = colori_richiesti[colore_random];

    // rileva posizione del puntatore del mouse
    // ho usato questa strategia perché VSC mi segnalava
    // che la proprietà «event» è deprecata
    document.onmousemove = function(e)
        {
            ascissa = e.pageX;
            ordinata = e.pageY;
        }

    // trova randomicamente le dimensioni del triangolo
    let larghezza = Math.random() * (max - min) + min;
    let altezza = Math.random() * (max - min) + min;
         
    // crea elemento div da inserire nel DOM
    const rettangolo = document.createElement("div");

    // definisci contenuto del div prima creato
    // il valore sottratto all'ordinata
    // serve a coprire l'altezza del box superiore
    rettangolo.innerHTML = 
    `
        <div 
            id="rettangolo${rettangolo_numero}"
            class="rettangolo"
            style=
            "
                width: ${larghezza}px;
                height: ${altezza}px;
                background-color: ${colore};
                top: ${ordinata-boxSuperiore}px;
                left: ${ascissa}px;
            "
        ">${rettangolo_numero}</div>
    `

    // quando l'utente fa click su un rettangolo esistente
    // rimuovilo dal DOM
    // e imposta la variabile booelana di appoggio a false
    // per indicare che si è cliccato su un rettangolo esistente
    // e non su un punto vuoto della cornice
    rettangolo.addEventListener("click", ()=> 
    {
        rettangolo.remove();
        vuoto = false;
    })

    // se l'utente ha cliccato su un punto vuoto della cornice
    // allora disegna il rettangolo 
    if (vuoto) 
    {
        // «appendi» rettangolo alla cornice
        cornice.appendChild(rettangolo);
        
        // quando disegna un rettangolo, incrementa il relativo contatore 
        rettangolo_numero++;
    }

    // in mancanza di altre condizioni, si suppone che l'utente
    // clicchi su un punto vuoto della cornice
    vuoto = true;
}

// rileva e gestisce click dell'utente sulla cornice 
cornice.addEventListener("click", disegnaRettangolo);